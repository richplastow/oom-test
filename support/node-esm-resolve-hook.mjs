//// https://nodejs.org/api/esm.html#esm_resolve_hook

//// Map shimmed modules to Node builtins.
const nodeBuiltins = {
    'oom-test-assert/all.mjs': 'assert'
  , 'fs.mjs':     'fs'
}

//// Set the default for the `parentModuleURL` argument.
const baseURL = new URL('file://')
baseURL.pathname = `${process.cwd()}/`

//// Define the resolve hook.
export async function resolve(
    specifier
  , parentModuleURL = baseURL
  , defaultResolver
) {

    //// Set
    let format = 'esm' // only kept if specifier is 'some/random-module.mjs'
      , url = new URL(specifier, parentModuleURL).href // only kept if specifier is 'some/random-module.mjs'
      , specifierSplit = specifier.split('/') // eg ['.','src','oom-test-assert','all.mjs']
      , specifierBase = specifierSplit.pop() // eg 'all.mjs'
      , specifierContainer = specifierSplit.pop() // eg 'oom-test-assert'

    ////
    const nodeBuiltin = nodeBuiltins[specifierContainer + '/' + specifierBase]
    if (nodeBuiltin)
        format = 'builtin', url = nodeBuiltin

    //// Return a special object which tells Node how to resolve each module.
    //// For 'some/random-module.mjs' it’s the same as if we hadn’t have hooked the resolver.
    //// For shimmed modules, './node_modules/oom-shim/fs.mjs', it’s the builtin 'fs'.
    return { format, url }
}
