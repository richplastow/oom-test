//// Load dependencies.
import assert from '../src/oom-test-assert/all.mjs'
import OomTestRunner from '../src/oom-test-runner/all.mjs'

//// This usage example omits `hub` in the constructor options, so OomTestRunner
//// creates its own event hub. In real-world usage you might pass in your app’s
//// own event hub.
const runner = new OomTestRunner({
    hub: null // if set, must be an object with fire() and on() methods
  , title: 'OomTestRunner Usage Example' // the main title
  // , skip: 'Why not?!'
  // , first: 1
  // , last: 2
  // , before () { console.log('runner.before()') } // called once, before beforeEach() for the first suite is run
  // , after () { console.log('runner.after()') } // called once, after the afterEach() for the last suite has completed
  // , beforeEach (t) { t.inc = t.inc || 0; t.inc++; t.beforeInc = t.beforeInc || 0; t.beforeInc++;  console.log('runner.beforeEach()', t.inc, t.beforeInc, t.afterInc ) } // called before every suite begins, after this runner’s before()
  // , afterEach (t) { t.inc++; t.afterInc = t.afterInc || 0; t.afterInc++;  console.log('runner.afterEach()', t.inc, t.beforeInc, t.afterInc ) } // called after each suite completes, before this runner’s after()
})

//
// console.log('assert', assert+'')

//// EVENTS

//// Browser logs all events except updates. Node doesn’t log any events.
if ('object' === typeof document)
    runner.on(
        'runner-start runner-end suite-start suite-end case-start case-end'
      , (...args) => console.log.apply( console.log, ['runner event:'].concat(args) )
    )

//// Each time the results update, log the result as a TAP string.
runner.on('case-start case-end suite-end suite-update runner-end runner-update', evt => {
    if ('object' !== typeof document) return // Node.js
    document.querySelector('pre#info').innerHTML = 'UPDATE\n'
    runner.tap.split('\n').map( line => console.info(line) )
})

//// When the test run has completed, log the result as a TAP string.
runner.on('runner-end', evt => {
    if ('object' === typeof document)
        document.querySelector('pre#info').innerHTML = 'END\n'
    runner.tap.split('\n').map( line => console.info(line) )
})




//// SUITES

//// Define keywords.
const [ SEQUENTIAL, PARALLEL ] = [ true, true ]

//// Create a couple of test suites.
const suite1 = runner.add({
    title: '1st suite, sequential'
  , SEQUENTIAL
  , first: 2
  , last: 3
  // , skip: 'Why not?!'
  // , before (t) { t.inc = t.inc || 0; t.inc++; t.beforeInc = t.beforeInc || 0; t.beforeInc++;  console.log('suite1.before()', t.inc, t.beforeInc, t.afterInc ) } // called once, before the first case is run
  // , after (t) { t.inc++; t.afterInc = t.afterInc || 0; t.afterInc++;  console.log('suite1.after()', t.inc, t.beforeInc, t.afterInc ) } // called once, after the last case has completed
  // , beforeEach (t) { t.inc = t.inc || 0; t.inc++; t.beforeInc = t.beforeInc || 0; t.beforeInc++;  console.log('suite1.beforeEach()', t.inc, t.beforeInc, t.afterInc ) } // called before every case begins, after this suite’s before()
  // , afterEach (t) { t.inc++; t.afterInc = t.afterInc || 0; t.afterInc++;  console.log('suite1.afterEach()', t.inc, t.beforeInc, t.afterInc ) } // called after each case completes, before this suite’s after()
})
const suite2 = runner.add({ // no before of after for this suite
    title: '2nd suite, parallel'
  // , skip: 'No reason, really'
  , first: 1
  , last: 3
  , PARALLEL
})

suite1.add(
    'Throws err after 700ms'
  , t => { setTimeout(() => t.notStrictEqual(1, 1, 'Err after 700ms'), 700) }

  , t => { setTimeout( () => {
        // for (const key of Object.keys(t))
        //     if (! assert[key]) {
        //         t[key] += 100
        //         console.log(`  ${key}: ${t[key]}`)
        //     }
        try {
            // function thrower () { throw Error('As expected!') }
            // thrower()
            // assert.throws(thrower)
            assert.strictEqual(1, 1)
        } catch (err) { t(err) }
        t('No error!')
      }
    , 700) }
  , 'Is fine immediately'
  , function () { 'Fine immediately' }
  , 'Throws err immediately'
  , function (t) { return assert.strictEqual('1', 1, 'Err immediately') }
  , 'Throws err after 400ms again'
  , function iHaveAName (t) {
        setTimeout( () => { t( Error('Err after 400ms again') ) }, 400 )
    }
)

suite1.add(
    'Is fine after 400ms'
  , function (t) {
        setTimeout( () => { t('Fine after 400ms') }, 400 )
    }
  , 'Throws another err after 400ms'
  , function err_after_200ms (t) {
        setTimeout( () => { t( Error('Another err after 400ms') ) }, 400 )
    }
)

suite2.add(
    'Is fine after 1800ms'
  , function (t) { setTimeout( () => { t('Fine after 1800ms') }, 1800 ) }
  , 'Is bad after 900ms'
  , t => setTimeout( () => { t( Error('Bad after 900ms') ) }, 900 )
  // , 'Is fine after 900ms'
  // , t => setTimeout( () => { t('Fine after 900ms') }, 900 )
  , '# skip Is fine after 300ms'
  , t => { setTimeout( () => {t('Fine after 300ms') }, 300 ) }
  , '# TODO Stop this from throwing err after 600ms'
  , function (t) { setTimeout( () => { t( Error('Err after 600ms')) }, 600 ) }
)
suite2.add(
    'Is fine immediately'
  , t => 'Is fine immediately'
  , 'Throws err immediately'
  , () => { throw Error('Err immediately') }
)


// suite2.add([ // array shorthand style
//     'Title for "wait is ok"'
//   , function wait (t) { t.somethingOk(); t('wait is ok') }
//   , 'Title for "wait oh no!"'
//   , function wait (t) { t.somethingWrong(); t(Error('wait oh no!')) }
//   , 'Title for "now is ok"'
//   , function now (t) { t.somethingOk(); return 'now is ok' }
//   , 'Title for "now oh no!"'
//   , function now (t) { t.somethingWrong(); throw Error('now oh no!') }
// ])

if ('object' === typeof document) {
    document.querySelector('pre#info').innerHTML = 'START\n'
    runner.tap.split('\n').map( line => console.info(line) )
}

// runner.run().then( thing => console.log(thing) )
setTimeout( () => runner.run(), 800 )
