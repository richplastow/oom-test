//// Polyfill Node.js’s `assert` for the browser.
import assert from '../src/oom-test-assert/all.mjs'
// import { strict as assert } from '../src/oom-test-assert/all.mjs'

//@TODO run tests from https://github.com/nodejs/node/tree/master/test/parallel
// console.log(assert);
// for (let key of Object.keys(assert).sort() )
//     console.log(key, typeof assert[key]);

// class SubErr extends Error {
//     constructor (msg) {
//         super(msg)
//     }
// }
//
// const err = new SubErr('ok yup')
// try {
//     throw err
// } catch (e) {
//     console.log('zz', e.message);
// }

console.info("\nassert.strict.equal(1, '1', 'note the strictness')");
try {
    assert.strict.equal(1, '1', 'note the strictness')
    console.warn('  Whoops! Expected an AssertionError!')
} catch (e) {
    console.log('  message:', e.message)
    for (let key of Object.keys(e).sort() )
        console.log(`  ${key}:`, e[key])
}

console.info("\nassert.strict.strictEqual(2, '2', 'would be strict anyway')");
try {
    assert.strict.strictEqual(2, '2', 'would be strict anyway')
    console.warn('  Whoops! Expected an AssertionError!')
} catch (e) {
    console.log('  message:', e.message)
    for (let key of Object.keys(e).sort() )
        console.log(`  ${key}:`, e[key])
}

console.info("\nassert.strict(0, 'zero is not truthy')");
try {
    // assert.strict(0)
    assert.strict(0, 'zero is not truthy')
    console.warn('  Whoops! Expected an AssertionError!')
} catch (e) {
    console.log('  message:', e.message)
    for (let key of Object.keys(e).sort() )
        console.log(`  ${key}:`, e[key])
}

// console.info("\nassert.strictEqual(1, '1')");
// try {
//     assert.strictEqual(1, '1')
//     console.warn('  Whoops! Expected an AssertionError!')
// } catch (e) {
//     console.log('  message:', e.message)
//     for (let key of Object.keys(e).sort() )
//         console.log(`  ${key}:`, e[key])
// }

// console.info("\nassert.equal(1, 2, '1 equals 2?!')");
// try {
//     assert.equal(1, 2, '1 equals 2?!')
//     console.warn('  Whoops! Expected an AssertionError!')
// } catch (e) {
//     console.log('  message:', e.message)
//     for (let key of Object.keys(e).sort() )
//         console.log(`  ${key}:`, e[key])
// }

// console.info("\nassert(false, '`false` is not truthy!')");
// try {
//     // assert(true, '`true` is truthy')
//     assert(false, '`false` is not truthy!')
//     // assert.ok(0)
//     console.warn('  Whoops! Expected an AssertionError!')
// } catch (e) {
//     console.log('  message:', e.message)
//     for (let key of Object.keys(e).sort() )
//         console.log(`  ${key}:`, e[key])
// }

// console.info("\nfail('boom')");
// try {
//     assert.fail('boom')
//     console.warn('  Whoops! Expected an AssertionError!')
// } catch (e) {
//     console.log('  message:', e.message)
//     for (let key of Object.keys(e).sort() )
//         console.log(`  ${key}:`, e[key])
// }
//
// console.info("\ndeepEqual({ a:1 }, { a:2 })")
// try {
//     assert.strict.deepEqual({ a:1 }, { a:true })
//     console.warn('  Whoops! Expected an AssertionError!')
// } catch (e) {
//     console.log('  message:', e.message)
//     for (let key of Object.keys(e).sort() )
//         console.log(`  ${key}:`, e[key])
// }
