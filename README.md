OomTest
=======

### A suite of Oomodules, including OomTestAssert, OomTestBench and OomTestUnit

[OomTest GitLab Project](https://gitlab.com/richplastow/oom-test) &nbsp;
[OomTest Homepage](https://richplastow.gitlab.io/oom-test/)


- [OomTestAssert][1] polyfills Node.js’s `assert` for the browser
- __OomTestBench__ provides benchmarking and performance tools
- __OomTestMain__ is a simple integration testing framework
- [OomTestRunner][2] is an evented test-runner, producing TAP results
- __OomTestUnit__ is a simple async unit testing framework


[1]: https://richplastow.gitlab.io/oom-test/support/oom-test-assert-usage.html
[2]: https://richplastow.gitlab.io/oom-test/support/oom-test-runner-usage.html
