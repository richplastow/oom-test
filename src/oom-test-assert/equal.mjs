//// https://nodejs.org/api/assert.html#assert_assert_equal_actual_expected_message
//// https://github.com/nodejs/node/blob/master/lib/assert.js

//// Load dependencies.
import { innerFail } from './utility.mjs'

//// Tests shallow, coercive equality between the `actual` and `expected`
//// arguments using the Abstract Equality Comparison (==).
//// Deprecated: use assert.strict.equal() or assert.strictEqual().
//// - actual <any>
//// - expected <any>
//// - message <string> | <Error>
export default function equal (actual, expected, message) {
    if (actual != expected) {
        innerFail({
            actual
          , expected
          , message
          , operator: '=='
          , stackStartFn: equal
        })
    }
}
