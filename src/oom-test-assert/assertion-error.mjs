//// https://nodejs.org/api/assert.html#assert_class_assert_assertionerror
//// https://github.com/nodejs/node/blob/master/lib/internal/assert.js#L288
export default class AssertionError extends Error {
    constructor (options) { //@TODO throw an ERR_INVALID_ARG_TYPE if no `options`
        const { actual, expected, message, operator, stackStartFn } = options

        //// Call Error’s constructor with an appropriate message.
        if (null != message)
            super(message+'')
          , this.generatedMessage = false
        else
            super('Generated message @TODO 1') //@TODO see github source
          , this.generatedMessage = true


        //// OPTIONS

        //// actual <any>
        //// The actual property on the error instance is going to contain this
        //// value. Internally used for the actual error input in case e.g.,
        //// assert.strictEqual() is used.
        this.actual = actual

        //// expected <any>
        //// The expected property on the error instance is going to contain
        //// this value. Internally used for the expected error input in case
        //// e.g., assert.strictEqual() is used.
        this.expected = expected

        //// message <string>
        //// If provided, the error message has been passed to Error.

        //// operator <string>
        //// The operator property on the error instance is going to contain
        //// this value. Internally used to indicate what operation was used for
        //// comparison (or what assertion function triggered the error).
        this.operator = operator

        //// stackStartFn <Function>
        //// If provided, the generated stack trace is going to remove all
        //// frames up to the provided function.
        // Error.captureStackTrace(this, stackStartFn) //@TODO works in Chrome but not Firefox - look at https://github.com/inf3rno/error-polyfill/blob/master/lib/non-v8/index.js




        //// READ-ONLY

        //// `code` and `name` are enumerable but read-only.
        Object.defineProperty(this, 'code', {
            enumerable: true
          , get () { return 'ERR_ASSERTION' }
        })
        Object.defineProperty(this, 'name', {
            enumerable: true
          , get () { return this.constructor.name + ` [${this.code}]` }
        })

    }
}
