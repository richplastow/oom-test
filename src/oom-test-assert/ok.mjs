//// https://nodejs.org/api/assert.html#assert_assert_ok_value_message
import AssertionError from './assertion-error.mjs'

//// Pure assertion tests whether a value is truthy, as determined by !!value.
export default function ok (...args) {
    innerOk(ok, args.length, ...args)
}


function getErrMessage(message, fn) {

    return 'Generated message @TODO 2' // @TODO rewrite getErrMessage() for the browser
/*
    // Make sure the limit is set to 1. Otherwise it could fail (<= 0) or it
    // does too much work.
    const tmpLimit = Error.stackTraceLimit
    Error.stackTraceLimit = 1

    // We only need the stack trace. To minimize the overhead use an object
    // instead of an error.
    const err = {}
    Error.captureStackTrace(err, fn)
    Error.stackTraceLimit = tmpLimit

    const tmpPrepare = Error.prepareStackTrace
    Error.prepareStackTrace = (_, stack) => stack
    const call = err.stack[0]
    Error.prepareStackTrace = tmpPrepare

    const filename = call.getFileName()

    if (! filename)
        return message

    const line = call.getLineNumber() - 1
    let column = call.getColumnNumber() - 1

    // Line number one reports the wrong column due to being wrapped in a
    // function. Remove that offset to get the actual call.
    if (line === 0) {
        if (columnOffset === 0) {
            const { wrapper } = require('internal/modules/cjs/loader')
            columnOffset = wrapper[0].length
        }
        column -= columnOffset
    }

    const identifier = `${filename}${line}${column}`

    if ( errorCache.has(identifier) ) {
        return errorCache.get(identifier)
    }

    // Skip Node.js modules!
    if ( filename.endsWith('.js') && NativeModule.exists(filename.slice(0, -3)) ) {
        errorCache.set(identifier, undefined)
        return
    }

    let fd
    try {
        // Set the stack trace limit to zero. This makes sure unexpected token
        // errors are handled faster.
        Error.stackTraceLimit = 0

        if (decoder === undefined) {
            const { StringDecoder } = require('string_decoder')
            decoder = new StringDecoder('utf8')
        }

        fd = openSync(filename, 'r', 0o666)
        // Reset column and message.
        [column, message] = getCode(fd, line, column)
        // Flush unfinished multi byte characters.
        decoder.end()
        // Always normalize indentation, otherwise the message could look weird.
        if (message.indexOf('\n') !== -1) {
            if (EOL === '\r\n') {
                message = message.replace(/\r\n/g, '\n')
            }
            const frames = message.split('\n')
            message = frames.shift()
            for (const frame of frames) {
                let pos = 0
                while (pos < column && (frame[pos] === ' ' || frame[pos] === '\t')) {
                    pos++
                }
                message += `\n  ${frame.slice(pos)}`
            }
        }
        message = `The expression evaluated to a falsy value:\n\n  ${message}\n`
        // Make sure to always set the cache! No matter if the message is
        // undefined or not
        errorCache.set(identifier, message)

        return message
    } catch (e) {
        // Invalidate cache to prevent trying to read this part again.
        errorCache.set(identifier, undefined)
    } finally {
        // Reset limit.
        Error.stackTraceLimit = tmpLimit
        if (fd !== undefined)
            closeSync(fd)
    }
    */
}


function innerOk (fn, argLen, value, message) {
    if (! value) {
        let generatedMessage = false

        if (argLen === 0) {
            generatedMessage = true
            message = 'No value argument passed to `assert.ok()`'
        } else if (message == null) {
            generatedMessage = true
            message = getErrMessage(message, fn)
        } else if (message instanceof Error) {
            throw message
        }

        const err = new AssertionError({
            actual: value
          , expected: true
          , message
          , operator: '=='
          , stackStartFn: fn
        })
        err.generatedMessage = generatedMessage //@TODO maybe get rid of this, `new AssertionError(...)` should set its own generatedMessage
        throw err
    }
}
