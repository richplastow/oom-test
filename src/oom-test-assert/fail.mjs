//// https://nodejs.org/api/assert.html#assert_assert_fail_message
//// https://github.com/nodejs/node/blob/master/lib/assert.js#L89

//// Load dependencies.
import AssertionError from './assertion-error.mjs'

//// Global state.
let warned = false //@TODO should be global

//// Throws an AssertionError with the provided error message or a default error
//// message. If the message parameter is an instance of an Error then it will
//// be thrown instead of the AssertionError.
//// Using assert.fail() with more than two arguments has been deprecated.
//// - message <string> | <Error> Default: 'Failed'
export default function fail (actual, expected, message, operator, stackStartFn) {
    const argsLen = arguments.length

    let internalMessage
    if (argsLen === 0) {
        internalMessage = 'Failed'
    } else if (argsLen === 1) {
        message = actual
        actual = undefined
    } else {
      if (warned === false) {
          warned = true
          console.warn( // was process.emitWarning()
              'assert.fail() with more than one argument is deprecated. Please '
                + 'use assert.strictEqual() instead or only pass a message.'
            , 'DeprecationWarning'
            , 'DEP0094'
          )
      }
      if (argsLen === 2) operator = '!='
    }

    if (message instanceof Error) throw message

    const errArgs = {
        actual
      , expected
      , operator: operator === undefined ? 'fail' : operator
      , stackStartFn: stackStartFn || fail
    }
    if (message !== undefined) {
        errArgs.message = message
    }
    const err = new AssertionError(errArgs)
    if (internalMessage) {
        err.message = internalMessage
        err.generatedMessage = true //@TODO maybe get rid of this, `new AssertionError(...)` should set its own generatedMessage
    }
    throw err
}
