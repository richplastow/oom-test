//// A toolkit of private utility methods, used internally by the public
//// `asset.*()` methods.

//// Load dependencies.
import AssertionError from './assertion-error.mjs'
//
// //// Export the utilities.
// export default {
//     innerFail
// }

//// Throws the message of an Error, if supplied, or otherwise instantiates and
//// throws an AssertionError. Used by strictEqual(), among others.
function innerFail (obj) {
    if (obj.message instanceof Error) throw obj.message
    throw new AssertionError(obj)
}

//// Export the utilities.
export {
    innerFail
}
