//// Aggregate the OomTestAssert source modules into a single object.
import AssertionError     from './assertion-error.mjs'
import deepEqual          from './deep-equal.mjs'
import deepStrictEqual    from './deep-strict-equal.mjs'
import doesNotReject      from './does-not-reject.mjs'
import doesNotThrow       from './does-not-throw.mjs'
import equal              from './equal.mjs'
import fail               from './fail.mjs'
import ifError            from './if-error.mjs'
import notDeepEqual       from './not-deep-equal.mjs'
import notDeepStrictEqual from './not-deep-strict-equal.mjs'
import notEqual           from './not-equal.mjs'
import notStrictEqual     from './not-strict-equal.mjs'
import ok                 from './ok.mjs'
import rejects            from './rejects.mjs'
import strictEqual        from './strict-equal.mjs'
import throws             from './throws.mjs'

const topLevelProps = {
    AssertionError
  , deepEqual: depreciated
  , deepStrictEqual
  , doesNotReject
  , doesNotThrow
  , equal
  , fail
  , ifError
  , notDeepEqual: depreciated
  , notDeepStrictEqual
  , notEqual: depreciated
  , notStrictEqual
  , ok
  , rejects
  , strictEqual
  , throws
}

const secondLevelProps = {
    AssertionError
  , deepEqual: deepStrictEqual
  , deepStrictEqual
  , doesNotReject
  , doesNotThrow
  , equal: strictEqual
  , fail
  , ifError
  , notDeepEqual: notDeepStrictEqual
  , notDeepStrictEqual
  , notEqual: notStrictEqual
  , notStrictEqual
  , ok
  , rejects
  , strictEqual
  , throws
}

function depreciated () {
    throw Error('That assertion is deprecated')
}

//// Node’s `assert` module is actually a function, an alias of `assert.ok()`.
//// https://nodejs.org/api/assert.html#assert_assert_value_message
const OomTestAssert = function (value, message) {
    return ok(value, message)
}
for (let propName in topLevelProps)
    OomTestAssert[propName] = topLevelProps[propName]

//// The top-level also has a property named `strict`. This is actually an alias
//// of `assert.ok()`. All of strict’s sub-properties run in strict mode, so
//// `assert.strict.equal()` and `assert.strict.strictEqual()` are identical.
OomTestAssert.strict = function (value, message) {
    return ok(value, message)
}
for (let propName in secondLevelProps)
    OomTestAssert.strict[propName] = secondLevelProps[propName]


export default OomTestAssert
