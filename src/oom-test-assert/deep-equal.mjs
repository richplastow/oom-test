//// https://nodejs.org/api/assert.html#assert_assert_deepequal_actual_expected_message
//// https://github.com/nodejs/node/blob/master/lib/assert.js

//// Tests for deep equality between the actual and expected parameters.
//// Primitive values are compared with the Abstract Equality Comparison (==).
export default function deepEqual (actual, expected, message) {
    throw Error('@TODO deepEqual()')
    // if (! isDeepEqual(actual, expected) ) {
    //     innerFail({
    //         actual
    //       , expected
    //       , message
    //       , operator: 'deepEqual'
    //       , stackStartFn: deepEqual
    //     })
    // }
}


const kLoose = false
const kStrict = true

function isDeepEqual (val1, val2) {
    return innerDeepEqual(val1, val2, kLoose)
}

function isDeepStrictEqual (val1, val2) {
    return innerDeepEqual(val1, val2, kStrict)
}

function innerDeepEqual (val1, val2, strict, memos) {
    // All identical values are equivalent, as determined by ===.
    if (val1 === val2) {
        if (val1 !== 0)
            return true
        return strict ? Object.is(val1, val2) : true
    }

    // Check more closely if val1 and val2 are equal.
    if (strict === true)
        return strictDeepEqual(val1, val2, memos)

    return looseDeepEqual(val1, val2, memos)
}


function looseDeepEqual(val1, val2, memos) {
    if (val1 === null || typeof val1 !== 'object') {
        if (val2 === null || typeof val2 !== 'object') {
            // eslint-disable-next-line eqeqeq
            return val1 == val2
        }
        return false
    }
    if (val2 === null || typeof val2 !== 'object') {
        return false
    }
    const val1Tag = objectToString(val1)
    const val2Tag = objectToString(val2)
    if (val1Tag === val2Tag) {
      if (isObjectOrArrayTag(val1Tag)) {
        return keyCheck(val1, val2, kLoose, memos, kNoIterator);
      }
      if (isArrayBufferView(val1)) {
        if (isFloatTypedArrayTag(val1Tag)) {
          return areSimilarFloatArrays(val1, val2);
        }
        return areSimilarTypedArrays(val1, val2);
      }
      if (isDate(val1) && isDate(val2)) {
        return val1.getTime() === val2.getTime();
      }
      if (isRegExp(val1) && isRegExp(val2)) {
        return areSimilarRegExps(val1, val2);
      }
      if (val1 instanceof Error && val2 instanceof Error) {
        if (val1.message !== val2.message || val1.name !== val2.name)
          return false;
      }
    // Ensure reflexivity of deepEqual with `arguments` objects.
    // See https://github.com/nodejs/node-v0.x-archive/pull/7178
    } else if (isArguments(val1Tag) || isArguments(val2Tag)) {
      return false;
    }
    if (isSet(val1)) {
      if (!isSet(val2) || val1.size !== val2.size) {
        return false;
      }
      return keyCheck(val1, val2, kLoose, memos, kIsSet);
    } else if (isMap(val1)) {
      if (!isMap(val2) || val1.size !== val2.size) {
        return false;
      }
      return keyCheck(val1, val2, kLoose, memos, kIsMap);
    } else if (isSet(val2) || isMap(val2)) {
      return false;
    }
    if (isAnyArrayBuffer(val1) && isAnyArrayBuffer(val2)) {
      if (!areEqualArrayBuffers(val1, val2)) {
        return false;
      }
    }
    return keyCheck(val1, val2, kLoose, memos, kNoIterator);
}
